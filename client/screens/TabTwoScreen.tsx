import React from "react";
import { StyleSheet, View } from "react-native";
import { MaterialIndicator } from "react-native-indicators";

const TabTwoScreen = () => {
  return (
    <View style={styles.container}>
      <MaterialIndicator color="red" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 70,
    flexDirection: "row",
  },
  activityIndicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 80,
    backgroundColor: "green",
  },
});

export default TabTwoScreen;
