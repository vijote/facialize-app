import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Button,
  Image,
  Modal,
} from "react-native";
import {
  Ionicons as Icon,
  SimpleLineIcons as RefreshIcon,
} from "@expo/vector-icons";
import { Camera } from "expo-camera";
import * as FileSystem from "expo-file-system";
import { StackNavigationProp } from "@react-navigation/stack";
import TextContent from "../constants/TextContent";
import { RootStackParamList } from "../types";
import { SafeAreaView } from "react-native-safe-area-context";
import { CapturedPicture } from "expo-camera/build/Camera.types";
import { MaterialIndicator } from "react-native-indicators";
import axios from "axios";

type CameraScreenProps = StackNavigationProp<RootStackParamList>;

type Props = {
  navigation: CameraScreenProps;
};

export default function TabOneScreen({ navigation }: Props) {
  const [hasPermission, setHasPermission] = useState(false);
  const [type, setType] = useState(Camera.Constants.Type.front);
  const [cameraReady, setCameraReady] = useState(false);
  const [sendingModal, setSendingModal] = useState(false);
  const [sentModal, setSentModal] = useState(false);
  const [picture, setPicture] = useState<CapturedPicture>({
    uri: "",
    width: 0,
    height: 0,
  });
  const camera = useRef<Camera>(null);

  const askPermissions = async () => {
    const { status } = await Camera.requestPermissionsAsync();
    setHasPermission(status === "granted");
  };

  const takePicture = async () => {
    if (camera.current) {
      try {
        const picture = await camera.current.takePictureAsync();
        setPicture(picture);
        console.log(picture);
        // vamos a obtener un objeto como el siguiente
        //   Object {
        //     "height": 3264,
        //     "uri": "file:///data/user/0/host.exp.exponent/cache/ExperienceData/%2540vijote%252Ffacialize/Camera/9224384e-031d-4a78-8552-be711391fcb9.jpg",
        //     "width": 2448,
        //   }
        // dividimos "uri" en diferentes strings por cada "/"
        let name: string[] | string = picture.uri.split("/");
        name = name[name.length - 1];

        let image = {
          uri: picture.uri,
          type: "image/jpeg",
          name,
        };

        const data = new FormData();

        data.append("file", image);
        data.append("upload_preset", "reconocimiento_facial");
        data.append("cloud_name", "dsb6w29lm");

        const file = await axios.post(
          "https://api.cloudinary.com/v1_1/dsb6w29lm/image/upload",
          data
        );
        console.log("todo bien pa \n", file.data.secure_url);
      } catch (error) {
        console.log("exploto todo pa");
      }
    }
  };

  useEffect(() => {
    (async () => {
      await askPermissions();
    })();
  }, []);

  useEffect(() => {
    if (picture.uri.length) {
      setSendingModal(true);
      setTimeout(() => {
        setSentModal(true);
      }, 1500);
      setTimeout(() => {
        setSendingModal(false);
        setSentModal(false);
      }, 3000);
      setTimeout(() => {
        setPicture({
          uri: "",
          width: 0,
          height: 0,
        });
      }, 3000);
    }
  }, [picture]);

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return (
      <View style={styles.errorContainer}>
        <Text style={{ marginBottom: "5%" }}>
          {TextContent.cameraAccessError}
        </Text>
        <Button
          title={TextContent.cameraAccessButtonText}
          onPress={askPermissions}
        />
      </View>
    );
  }
  return (
    <View style={styles.container}>
      {picture.uri ? (
        <Image
          source={picture}
          style={{ maxHeight: "100%", maxWidth: "100%" }}
        />
      ) : (
        <Camera
          style={styles.camera}
          type={type}
          ref={camera}
          onCameraReady={() => setCameraReady(true)}
        >
          <View style={styles.buttonContainer}>
            <View style={styles.labelContainer}>
              <Text style={styles.label}>Saque una foto para registrarse</Text>
            </View>
            {cameraReady && (
              <TouchableOpacity
                style={styles.pictureButton}
                onPress={takePicture}
              >
                <Icon name="camera" color="#fefefe" size={40} />
              </TouchableOpacity>
            )}
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                setType(
                  type === Camera.Constants.Type.back
                    ? Camera.Constants.Type.front
                    : Camera.Constants.Type.back
                );
              }}
            >
              <RefreshIcon name="refresh" color="#fefefe" size={40} />
            </TouchableOpacity>
          </View>
        </Camera>
      )}

      <Modal visible={sendingModal} transparent={true}>
        <SafeAreaView
          style={{
            backgroundColor: "rgba(0,0,0,0.5)",
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <View
            style={{
              backgroundColor: "#fefefe",
              height: 300,
              width: 250,
              borderRadius: 15,
            }}
          >
            <View>
              <Text>Enviando...</Text>
              <MaterialIndicator color="red" />
            </View>
          </View>
        </SafeAreaView>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  errorContainer: {
    flex: 1,
    justifyContent: "center",
    marginHorizontal: "5%",
  },
  camera: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "center",
    margin: 20,
    position: "relative",
  },
  labelContainer: {
    position: "absolute",
    alignItems: "center",
    top: 0,
    width: "100%",
    backgroundColor: "rgba(0,0,0,0.45)",
    borderRadius: 15,
  },
  label: {
    color: "#fefefe",
    fontSize: 20,
    paddingVertical: 10,
  },
  button: {
    alignSelf: "flex-end",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.45)",
    borderWidth: 2,
    borderColor: "#fefefe",
    borderRadius: 1000,
    padding: 10,
    position: "absolute",
    left: 0,
  },
  pictureButton: {
    alignSelf: "flex-end",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.45)",
    borderWidth: 2,
    borderColor: "#fefefe",
    borderRadius: 1000,
    padding: 10,
  },
  text: {
    fontSize: 18,
    color: "white",
  },
});
