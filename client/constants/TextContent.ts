export default {
  tabOneTitle: "Registrar Asistencia",
  tabTwoTitle: "Informe Asistencia",

  bottomTabOneTitle: "Asistencia",
  bottomTabTwoTitle: "Informe",

  cameraAccessError: "No se puede acceder a la camara",
  cameraAction: "Sacar Foto",
  cameraAccessButtonText: "Habilitar camara",
};
