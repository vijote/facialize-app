"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var faces_1 = require("../controllers/faces");
var router = express_1.Router();
router.post("/upload");
router.post("/compare");
router.get("/attendance", faces_1.checkAttendance);
exports.default = router;
