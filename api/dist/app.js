"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var faces_1 = __importDefault(require("./routes/faces"));
var app = express_1.default();
app.use("/faces", faces_1.default);
app.use(function (err, req, res, next) {
    res.json({
        message: err.message,
    });
});
app.listen(3000);
console.log("server listening port 3000");
