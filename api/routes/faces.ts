import { Router } from "express";
import { checkAttendance } from "../controllers/faces";

const router = Router();

router.post("/upload");

router.post("/compare");

router.get("/attendance", checkAttendance);

export default router;
