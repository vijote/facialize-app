import express from "express";
import faceRoutes from "./routes/faces";

const app = express();

app.use("/faces", faceRoutes);

app.use(
  (
    err: Error,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    res.json({
      message: err.message,
    });
  }
);

app.listen(3000);
console.log("server listening port 3000");
